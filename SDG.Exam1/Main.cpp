// Simple Calculator
// Sawyer Grambihler

#include <iostream>
#include <conio.h>

using namespace std;

void Square(double, double &output);
void Cube(double, double &output);

int main() {
	char loop = 'y';
	while (loop == 'y') {
		//Get a number from the user
		double num;
		cout << "Enter a number\n";
		cin >> num;

		//Ask which operation to perform
		int operation;
		do
		{
			cout << "Enter \"2\" to square your number, or \"3\" to cube your number\n";
			cin >> operation;
		} while (operation != 2 && operation != 3);

		if (operation == 2)
		{
			double squared;
			Square(num, squared);
			cout << num << " squared = " << squared << "\n";
		}
		else
		{
			double cubed;
			Cube(num, cubed);
			cout << num << " cubed = " << cubed << "\n";
		}

		cout << "Enter \"Y\" to do another calculation, or anything else to exit\n";
		cin >> loop;
		loop = tolower(loop);
	}
}

void Square(double input, double &output) {
	output = input * input;
}

void Cube(double input, double& output) {
	output = input * input * input;
}
